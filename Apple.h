#import <Foundation/Foundation.h>


@interface Apple : NSObject

 {
	 NSNumber * numberOfStones;
	 NSString * color;
	  BOOL isFallen;
  }

  @property(retain) NSNumber * numberOfStones;

  @property(retain) NSString * color;
  
  @property BOOL isFallen;

  - (void) mature;
  
  - (void) fall;
  
   - (id) init ;
  
@end
