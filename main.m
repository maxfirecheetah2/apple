#import <Foundation/Foundation.h>
#import "Apple.h"

int main (int argc, const char * argv[])
{
        NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
        NSLog (@"Hello, World!");
		
		Apple * apple1 = [[Apple alloc] init];
	
    	NSLog(@" %s", [apple1 isFallen] ? "true" : "false");
		[apple1 fall];
		NSLog(@" %s", [apple1 isFallen] ? "true" : "false");
		
        [pool drain];
        return 0;
}