#import "Apple.h"

@implementation Apple

  @synthesize  color;
  
  @synthesize  isFallen;
  
  @synthesize  numberOfStones;

	- (void) fall 
	{
		self.isFallen = true;
	}
	
	- (void) mature
	{
		self.color = @"Red";
	}
	
	- (id) init 
	{
		self = [super init];
		
		self.isFallen = false;
		self.color = @"Green";
	
       return self;
    }


@end